package com.groupnine.liveprofile;

public class Patient {
	
	private String patientID;
	private String firstName;
	private String middleName;
	private String lastName;
	private int age;
	private double weight;
	private String gender;
	private String[] medications;
	private String[] allergies;
	private String[] medicalHistory;
	private String status; 

	
	public Patient(String _patientID, String _first, String _middle, String _last, int _age, double _weight, String _gender, String[] _medications, String[] _allergies, String[] _medicalHistory, String _status){
		this.patientID = new String(_patientID);
		this.age = _age;
		this.firstName = _first;
		this.middleName = _middle;
		this.lastName = _last;
		this.weight = _weight;
		this.gender = new String(_gender);
		this.medications = _medications;
		this.allergies = _allergies;
		this.medicalHistory = _medicalHistory;
		this.status = new String(_status);
	}
	
	public Patient(String _patientID, String _first, String _middle, String _last, int _age, double _weight, String _gender, String _status){
		patientID = new String(_patientID);
		age = _age;
		weight = _weight;
		gender = new String(_gender);
		status = new String(_status);
		this.firstName = _first;
		this.middleName = _middle;
		this.lastName = _last;
	}
	
	
	public String getPatientID(){
		return patientID;
	}
	
	public int getAge(){
		return age;
	}
	
	public double getWeight(){
		return weight;
	}
	
	public String getGender(){
		return gender;
	}
	
	public String[] getMedications(){
		return medications;
	}
	
	public String[] getAllergies(){
		return allergies;
	}
	
	public String[] getMedicalHistory(){
		return medicalHistory;
	}
	
	public String getStatus(){
		return status;
	}
	
	public String getFirstName(){
		return firstName;
	}
	public String getMiddleName(){
		return middleName;
	}
	public String getLastName(){
		return lastName;
	}
	
	
	public String toString(){
		
		return new String("First Name: " + firstName +"\nMiddle Name: " + middleName + "\nLastName: " + 
						lastName + "\nAge: " + age + "; Weight: " + weight + " lbs; Gender: " + gender + "\nPatient ID: "
						+ patientID + "\nStatus: " + status + "");
		
		
	}
	
}
